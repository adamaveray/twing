import TestBase from "../../TestBase";

export default class extends TestBase {
    getDescription() {
        return 'empty comments';
    }

    getTemplates() {
        return {
            'index.twig': `
Completely empty comment tag:
{##}

Single space empty comment tag:
{# #}

Single newline empty comment tag:
{#
#}

Empty comment tag:
{#   #}
`
        };
    }

    getExpected() {
        return `
Completely empty comment tag:

Single space empty comment tag:

Single newline empty comment tag:

Empty comment tag:
`;
    }

    getContext() {
        return {}
    }
}
